# Backend

![Docker build](https://github.com/List-Rocket/Backend/actions/workflows/docker-build.yml/badge.svg)
![Docker Compose check](https://github.com/List-Rocket/Backend/actions/workflows/docker-compose-check.yml/badge.svg)
![Node.js](https://github.com/List-Rocket/Backend/actions/workflows/nodejs.yml/badge.svg)



# Start dev server

```
docker-compose -f docker-compose.dev.yml up --build
```

# Run prod server
```
docker-compose -f docker-compose.yml up --build
```

# Run test
```
make test
```