import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

function NonConnected() {
  return (
    <div className="App">
        <h1> You need to be connected to access this page</h1>
        <div>
            <a href="/login">Click here to connect to the site web</a>
        </div>
    </div>
  );
}
export default NonConnected;