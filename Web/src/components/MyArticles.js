import axios from 'axios';
import React, {useEffect, useState} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";

function MyArticles() {
    const [content, setContent] = useState(null);

    const removeArticle = (article_id, author_id) => {
            axios.post("http://localhost:5000/DeleteArticle", {
            article: article_id,
            author: author_id,
        })
            .then((e) => {
                console.log(e)
                alert(e)
              window.location.reload(true)
        })
            .catch((error) => {
           console.log(error);
        });
    }

    useEffect(() => {
        axios.get("http://localhost:5000/MyArticle/" + localStorage.getItem("userId"))
            .then((e) => {
                console.log(e);
                setContent(e.data)
        })
            .catch((error) => {
            alert("An error occured")
           console.log(error);
        });
    }, [])

    return (
        <div>
            {content !== null && content.length !== 0 ? (content.map((key, index) => {
                return (
                    <div className="card" style={{ marginBottom: "10px" }} key={index}>
                        <div className="card-header">
                            {key.title}
                            <div style={{ float: "right", display: "inline-flex"}}>
                                <p className="card-text" style={{ paddingRight: "8px", fontSize: "21px"}} >{ key.like_nb}</p>
                                <button type="button" className="btn btn-primary btn-rounded" style={{ "backgroundColor": "green", width: "35px", height: "35px", padding: "0px"}}>
                                    <img src="https://icones.pro/wp-content/uploads/2021/04/icone-noire-noir.png" style={{ width: "30px", height: "30px" }}/>
                                </button>
                                <button type="button" className="btn btn-danger btn-rounded" style={{ marginLeft: "10px", width: "35px", height: "35px", padding: "0px"}} onClick={ () => removeArticle(key.id, key.author) } >
                                    <img src="https://icons-for-free.com/download-icon-delete+remove+trash+trash+bin+trash+can+icon-1320073117929397588_512.png" style={{ width: "30px", height: "30px" }}/>
                                </button>
                            </div>
                        </div>
                            <div className="card-body">
                            <h5 className="card-title">{ key.text}</h5>
                            <p className="card-text">{ key.authorUsername }</p>
                        </div>
                    </div>
                )
            })): <></>}
        </div>
    );
}

export default MyArticles;