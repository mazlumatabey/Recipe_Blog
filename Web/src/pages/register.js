import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import '../App.css';

function Register() {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [passwordError, setpasswordError] = useState("");
  const [emailError, setemailError] = useState("");
  const [usernameError, setuserenameError] = useState("");
  const history = useHistory();

  const handleValidation = (event) => {
    let formIsValid = true;

    if (!email.match(/^\w.+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      formIsValid = false;
      setemailError("Email Not Valid");
      return false;
    } else {
      setemailError("");
      formIsValid = true;
    }

    if (!password.match(/^[a-zA-Z0-9]{8,22}$/)) {
      formIsValid = false;
      setpasswordError(
        "Length must be between 8 and 22 Chracters"
      );
      return false;
    } else {
      setpasswordError("");
      formIsValid = true;
    }
    if (username.length < 4 || username.length > 16) {
      formIsValid = false;
      setuserenameError(
        "Length must be between 4 and 16 Chracters"
      );
      return false;
    } else {
      setuserenameError("");
      formIsValid = true;      
    }
    /* ************************************
    ***************************************
    ***************************************
    ******* THIS IS HOW WE CALL BACKEND ***
    ***************************************
    ***************************************/
    axios.post("http://localhost:5000/user", {
      email: email,
      password: password,
      username: username,
    })
      .then((e) => {
        alert("Account created, you will be redirected to login page ...")
        history.push('/login');
    })
      .catch((error) => {
      alert("An error occured")
      console.log(error);
    });
    /* ************************************
    ***************************************
    ***************************************/
    
    return formIsValid;
  };

  const loginSubmit = (e) => {
    e.preventDefault();
    handleValidation();
    console.log(email)
    console.log(password)
  };

  return (
    <div className="my-forms">
      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-md-4" style={{ borderRadius: "30px", backgroundColor: "#f1f7fe", padding: "30px" }}>
          <h1 className="page-header">Register</h1>
            <form id="register" onSubmit={loginSubmit}>
            <div className="form-group">
                <label>Email address</label>
                <input
                  type="email"
                  className="form-control"
                  id="EmailInput"
                  name="EmailInput"
                  aria-describedby="emailHelp"
                  placeholder="Enter email"
                  onChange={(event) => setEmail(event.target.value)}
                />
                <small id="emailHelp" className="text-danger form-text">
                  {emailError}
                </small>
              </div>
              <div className="form-group">
                <label>Username</label>
                <input
                  type="text"
                  className="form-control"
                  id="UsernameInput"
                  name="UsernameInput"
                  aria-describedby="usernameHelp"
                  placeholder="Enter username"
                  onChange={(event) => setUsername(event.target.value)}
                />
                <small id="usernameHelp" className="text-danger form-text">
                  {usernameError}
                </small>
              </div>
              <div className="form-group">
                <label>Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  onChange={(event) => setPassword(event.target.value)}
                />
                <small id="passworderror" className="text-danger form-text">
                  {passwordError}
                </small>
              </div>
              <button type="submit" className="btn btn-primary">
                Register
              </button>
              <div>                              
                    <a href="/login">Already have an account ?</a>
                </div>
            </form>
          </div>
        </div>
      </div>
      </div>
  );
}
export default Register;

