import axios from 'axios';
import React, {useEffect, useState} from 'react';
import "./Profile.css";
import MyArticles from '../components/MyArticles';

function Profile() {
    const [username, setUsername] = useState(localStorage.getItem("username"))
    const [password, setPassword] = useState("");
    const [passwordV, setPasswordV] = useState("");
    const [passwordError, setpasswordError] = useState("");
    const [passwordError2, setpasswordError2] = useState("");
    const [usernameError, setuserenameError] = useState("");

    const handleValidation = (event) => {
        let formIsValid = true;

        if (password !== "") {
            if (!password.match(/^[a-zA-Z0-9]{8,22}$/)) {
                formIsValid = false;
                setpasswordError(
                    "Length must be between 8 and 22 Chracters"
                );
                return false;
            } else {
                setpasswordError("");
                formIsValid = true;
            }
        }

        if (password !== passwordV) {
            formIsValid = false;
            setpasswordError2("Different passwords typed");
            return false;
        } else {
            setpasswordError2("");
            formIsValid = true;
        }

        if (username.length < 4 || username.length > 16) {
            formIsValid = false;
            setuserenameError(
              "Length must be between 4 and 16 Chracters"
            );
            return false;
        } else {
            setuserenameError("");
            formIsValid = true;      
        }

        axios.post("http://localhost:5000/update", {
            username: username,
            password: password,
            id: localStorage.getItem("userId")
        })
            .then((e) => {
              localStorage.setItem("username", e.data)
              window.location.reload(true)
        })
            .catch((error) => {
            alert("An error occured")
           console.log(error);
        });

        return formIsValid;
    }

    const submitChanges = (e) => {
        e.preventDefault();
        handleValidation();
    }

    return (
          <div className="Profile">
      <div className="ProfileWrapper">
        <div className='divContainer'>
            <form className="ProfileForm" id="userinfosform" onSubmit={submitChanges}>
                <label>Username</label>
                <input type="text" value={username} placeholder={username} onChange={(e) => setUsername(e.target.value)} name="nameProfile" />
                <small id="usernameerror" className="text-danger form-text">{usernameError}</small>
                <label>Password</label>
                <input type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} name="passwordProfile" />
                <small id="passworderror" className="text-danger form-text">{passwordError}</small>
                <label>Type password again</label>
                <input type="password" placeholder="Type the new password again please" onChange={(e) => setPasswordV(e.target.value)} name="repasswordProfile" />
                <small id="passworderror2" className="text-danger form-text">{passwordError2}</small>
                <button className="settingsSubmitButton" type="submit">
                    Update
                </button>
            </form>
            <h2 className="centerHeader"> My Posts : </h2>    
        </div>
        <MyArticles></MyArticles>
    </div>
    </div>
    );
}

export default Profile;