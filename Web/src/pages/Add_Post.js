import React, {Component, useState} from 'react';
import NonConnected from '../components/notConnected';
import axios from "axios";
import {Button, Form, FormControl, FormGroup} from "react-bootstrap";

function AddPost() {
    const [connected, setConnected] = useState(localStorage.getItem("userId"));
    const [title, setTitle] = useState("");
    const [text, setText] = useState("");
    const [textError, setTextError] = useState("");
    const [titleError, setTitleError] = useState("");


    const handleValidation = (event) => {
    let formIsValid = true;


    if (title.length < 5 || title.length > 32) {
      formIsValid = false;
      setTitleError(
        "Length must be between 5 and 32 characters"
      );
      return false;
    } else {
        setTitleError("");
        formIsValid = true;
    }
    if (text.length < 5) {
      formIsValid = false;
      setTextError(
        "Length must be longer than 5 characters"
      );
      return false;
    } else {
      setTextError("");
      formIsValid = true;
    }
    /* ************************************
    ***************************************
    ***************************************
    ******* THIS IS HOW WE CALL BACKEND ***
    ***************************************
    ***************************************/
    axios.post("http://localhost:5000/Article", {
      title: title,
      text: text,
      author_id: localStorage.getItem("userId"),
    })
      .then((e) => {
        alert("Article created")
        window.location.reload(true);
    })
      .catch((error) => {
      alert("An error occured")
      console.log(error);
    });
    /* ************************************
    ***************************************
    ***************************************/

    return formIsValid;
  };

  const articleAddSubmit = (e) => {
    e.preventDefault();
    handleValidation();
    console.log(title)
    console.log(text)
  };


    return (
        <div className='App_Post'>
            {connected !== null ? <></> : <NonConnected/>}
            <h1>Article</h1>
            <Form onSubmit={articleAddSubmit}>
                    <label className={"me-0"}>
                        Title
                    </label>
                    <FormControl
                        type={"text"}
                        placeholder={"My best cake ever!"}
                        className={"row-mb-3 "}
                        onChange={(event) => setTitle(event.target.value)}
                    />
                    <small id="emailHelp" className="text-danger form-text">
                      {titleError}
                    </small>
                    <label className={"me-0"}>
                        Description
                    </label>
                    <FormControl
                        type={"text"}
                        placeholder={"How I did it?"}
                        className={"mb-3"}
                        onChange={(event) => setText(event.target.value)}
                    />
                    <small id="emailHelp" className="text-danger form-text">
                      {textError}
                    </small>
                    <Button type={"submit"} className={"btn btn-primary"}>
                        Submit
                    </Button>
            </Form>
        </div>
    );
}

export default AddPost;
