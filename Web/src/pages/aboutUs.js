import React, {Component, useState} from 'react';
import NonConnected from '../components/notConnected';

function AboutUs() {
    const [connected, setConnected] = useState(localStorage.getItem("userId"));

    return (
        <div className='App'>
            <h1>AboutUs</h1>
            {connected !== null ? <></> : <NonConnected/>}
        </div>
    );
}

export default AboutUs;